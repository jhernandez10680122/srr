<?php

namespace proyecto\ejemploBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * grupos
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="proyecto\ejemploBundle\Entity\gruposRepository")
 */
class grupos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="clave", type="integer")
     */
    private $clave;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="periodo", type="string", length=50)
     */
    private $periodo;

    /**
     * @var integer
     *
     * @ORM\Column(name="anio", type="integer")
     */
    private $anio;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxi_alu", type="integer")
     */
    private $maxiAlu;


    
    /**
    * @ORM\ManyToOne(targetEntity="profesores", inversedBy="profesor")
    * @ORM\JoinColumn(name="idProfesor",referencedColumnName="id")
    */
    private $idProfesor;

    
    /**
    * @ORM\ManyToOne(targetEntity="materias", inversedBy="materia")
    * @ORM\JoinColumn(name="idMateria",referencedColumnName="id")
    */
    private $idMateria;


    
    /**
    * @ORM\OneToMany(targetEntity="grupoAlumno", mappedBy="grupos")
    */
    private $grupo;
    
    public function __construct(){
        $this->grupo = new \Doctrine\Common\Collections\ArrayCollection();
    }    




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clave
     *
     * @param integer $clave
     * @return grupos
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return integer 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return grupos
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set periodo
     *
     * @param string $periodo
     * @return grupos
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return string 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return grupos
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set maxiAlu
     *
     * @param integer $maxiAlu
     * @return grupos
     */
    public function setMaxiAlu($maxiAlu)
    {
        $this->maxiAlu = $maxiAlu;

        return $this;
    }

    /**
     * Get maxiAlu
     *
     * @return integer 
     */
    public function getMaxiAlu()
    {
        return $this->maxiAlu;
    }

    /**
     * Set idProfesor
     *
     * @param \proyecto\ejemploBundle\Entity\profesores $idProfesor
     * @return grupos
     */
    public function setIdProfesor(\proyecto\ejemploBundle\Entity\profesores $idProfesor = null)
    {
        $this->idProfesor = $idProfesor;

        return $this;
    }

    /**
     * Get idProfesor
     *
     * @return \proyecto\ejemploBundle\Entity\profesores 
     */
    public function getIdProfesor()
    {
        return $this->idProfesor;
    }

    /**
     * Set idMateria
     *
     * @param \proyecto\ejemploBundle\Entity\materias $idMateria
     * @return grupos
     */
    public function setIdMateria(\proyecto\ejemploBundle\Entity\materias $idMateria = null)
    {
        $this->idMateria = $idMateria;

        return $this;
    }

    /**
     * Get idMateria
     *
     * @return \proyecto\ejemploBundle\Entity\materias 
     */
    public function getIdMateria()
    {
        return $this->idMateria;
    }

    /**
     * Add grupo
     *
     * @param \proyecto\ejemploBundle\Entity\grupoAlumno $grupo
     * @return grupos
     */
    public function addGrupo(\proyecto\ejemploBundle\Entity\grupoAlumno $grupo)
    {
        $this->grupo[] = $grupo;

        return $this;
    }

    /**
     * Remove grupo
     *
     * @param \proyecto\ejemploBundle\Entity\grupoAlumno $grupo
     */
    public function removeGrupo(\proyecto\ejemploBundle\Entity\grupoAlumno $grupo)
    {
        $this->grupo->removeElement($grupo);
    }

    /**
     * Get grupo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupo()
    {
        return $this->grupo;
    }
}
