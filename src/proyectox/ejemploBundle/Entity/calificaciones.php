<?php

namespace proyecto\ejemploBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * calificaciones
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="proyecto\ejemploBundle\Entity\calificacionesRepository")
 */
class calificaciones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="grupoAlumno", inversedBy="gp")
    * @ORM\JoinColumn(name="idGp",referencedColumnName="id")
    */
    private $idGp;

    /**
     * @var integer
     *
     * @ORM\Column(name="u1", type="integer")
     */
    private $u1;
    
        /**
     * @var integer
     *
     * @ORM\Column(name="u2", type="integer")
     */
    private $u2;
    
    
        /**
     * @var integer
     *
     * @ORM\Column(name="u3", type="integer")
     */
    private $u3;


    /**
     * @var integer
     *
     * @ORM\Column(name="u4", type="integer")
     */
    private $u4;


    /**
     * @var integer
     *
     * @ORM\Column(name="u5", type="integer")
     */
    private $u5;


    /**
     * @var integer
     *
     * @ORM\Column(name="u6", type="integer")
     */
    private $u6;


    /**
     * @var integer
     *
     * @ORM\Column(name="u7", type="integer")
     */
    private $u7;


    /**
     * @var integer
     *
     * @ORM\Column(name="u8", type="integer")
     */
    private $u8;

    /**
     * @var integer
     *
     * @ORM\Column(name="u9", type="integer")
     */
    private $u9;


    /**
     * @var integer
     *
     * @ORM\Column(name="promedio", type="integer")
     */
    private $promedio;


    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set u1
     *
     * @param integer $u1
     * @return calificaciones
     */
    public function setU1($u1)
    {
        $this->u1 = $u1;

        return $this;
    }

    /**
     * Get u1
     *
     * @return integer 
     */
    public function getU1()
    {
        return $this->u1;
    }

    /**
     * Set u2
     *
     * @param integer $u2
     * @return calificaciones
     */
    public function setU2($u2)
    {
        $this->u2 = $u2;

        return $this;
    }

    /**
     * Get u2
     *
     * @return integer 
     */
    public function getU2()
    {
        return $this->u2;
    }

    /**
     * Set u3
     *
     * @param integer $u3
     * @return calificaciones
     */
    public function setU3($u3)
    {
        $this->u3 = $u3;

        return $this;
    }

    /**
     * Get u3
     *
     * @return integer 
     */
    public function getU3()
    {
        return $this->u3;
    }

    /**
     * Set u4
     *
     * @param integer $u4
     * @return calificaciones
     */
    public function setU4($u4)
    {
        $this->u4 = $u4;

        return $this;
    }

    /**
     * Get u4
     *
     * @return integer 
     */
    public function getU4()
    {
        return $this->u4;
    }

    /**
     * Set u5
     *
     * @param integer $u5
     * @return calificaciones
     */
    public function setU5($u5)
    {
        $this->u5 = $u5;

        return $this;
    }

    /**
     * Get u5
     *
     * @return integer 
     */
    public function getU5()
    {
        return $this->u5;
    }

    /**
     * Set u6
     *
     * @param integer $u6
     * @return calificaciones
     */
    public function setU6($u6)
    {
        $this->u6 = $u6;

        return $this;
    }

    /**
     * Get u6
     *
     * @return integer 
     */
    public function getU6()
    {
        return $this->u6;
    }

    /**
     * Set u7
     *
     * @param integer $u7
     * @return calificaciones
     */
    public function setU7($u7)
    {
        $this->u7 = $u7;

        return $this;
    }

    /**
     * Get u7
     *
     * @return integer 
     */
    public function getU7()
    {
        return $this->u7;
    }

    /**
     * Set u8
     *
     * @param integer $u8
     * @return calificaciones
     */
    public function setU8($u8)
    {
        $this->u8 = $u8;

        return $this;
    }

    /**
     * Get u8
     *
     * @return integer 
     */
    public function getU8()
    {
        return $this->u8;
    }

    /**
     * Set u9
     *
     * @param integer $u9
     * @return calificaciones
     */
    public function setU9($u9)
    {
        $this->u9 = $u9;

        return $this;
    }

    /**
     * Get u9
     *
     * @return integer 
     */
    public function getU9()
    {
        return $this->u9;
    }

    /**
     * Set promedio
     *
     * @param integer $promedio
     * @return calificaciones
     */
    public function setPromedio($promedio)
    {
        $this->promedio = $promedio;

        return $this;
    }

    /**
     * Get promedio
     *
     * @return integer 
     */
    public function getPromedio()
    {
        return $this->promedio;
    }

    /**
     * Set idGp
     *
     * @param \proyecto\ejemploBundle\Entity\grupoAlumno $idGp
     * @return calificaciones
     */
    public function setIdGp(\proyecto\ejemploBundle\Entity\grupoAlumno $idGp = null)
    {
        $this->idGp = $idGp;

        return $this;
    }

    /**
     * Get idGp
     *
     * @return \proyecto\ejemploBundle\Entity\grupoAlumno 
     */
    public function getIdGp()
    {
        return $this->idGp;
    }
}
