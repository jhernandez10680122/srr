<?php

namespace proyecto\ejemploBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class calificacionesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('u1')
            ->add('u2')
            ->add('u3')
            ->add('u4')
            ->add('u5')
            ->add('u6')
            ->add('u7')
            ->add('u8')
            ->add('u9')
            ->add('promedio')
            ->add('idGp')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'proyecto\ejemploBundle\Entity\calificaciones'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proyecto_ejemplobundle_calificaciones';
    }
}
