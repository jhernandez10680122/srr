<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use proyecto\ejemploBundle\Entity\grupos;
use proyecto\ejemploBundle\Form\gruposType;

/**
 * grupos controller.
 *
 * @Route("/grupos")
 */
class gruposController extends Controller
{

    /**
     * Lists all grupos entities.
     *
     * @Route("/", name="grupos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('proyectoejemploBundle:grupos')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new grupos entity.
     *
     * @Route("/", name="grupos_create")
     * @Method("POST")
     * @Template("proyectoejemploBundle:grupos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new grupos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grupos_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a grupos entity.
    *
    * @param grupos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(grupos $entity)
    {
        $form = $this->createForm(new gruposType(), $entity, array(
            'action' => $this->generateUrl('grupos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new grupos entity.
     *
     * @Route("/new", name="grupos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new grupos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a grupos entity.
     *
     * @Route("/{id}", name="grupos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:grupos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grupos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing grupos entity.
     *
     * @Route("/{id}/edit", name="grupos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:grupos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grupos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a grupos entity.
    *
    * @param grupos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(grupos $entity)
    {
        $form = $this->createForm(new gruposType(), $entity, array(
            'action' => $this->generateUrl('grupos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing grupos entity.
     *
     * @Route("/{id}", name="grupos_update")
     * @Method("PUT")
     * @Template("proyectoejemploBundle:grupos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:grupos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grupos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grupos_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a grupos entity.
     *
     * @Route("/{id}", name="grupos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('proyectoejemploBundle:grupos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find grupos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grupos'));
    }

    /**
     * Creates a form to delete a grupos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grupos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
