<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use proyecto\ejemploBundle\Entity\profesores;
use proyecto\ejemploBundle\Form\profesoresType;

/**
 * profesores controller.
 *
 * @Route("/profesores")
 */
class profesoresController extends Controller
{

    /**
     * Lists all profesores entities.
     *
     * @Route("/", name="profesores")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('proyectoejemploBundle:profesores')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new profesores entity.
     *
     * @Route("/", name="profesores_create")
     * @Method("POST")
     * @Template("proyectoejemploBundle:profesores:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new profesores();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('profesores_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a profesores entity.
    *
    * @param profesores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(profesores $entity)
    {
        $form = $this->createForm(new profesoresType(), $entity, array(
            'action' => $this->generateUrl('profesores_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new profesores entity.
     *
     * @Route("/new", name="profesores_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new profesores();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    
    
    public function new0Action()
    {
        return $this->render('proyectoejemploBundle:profesores:new0.html.twig');
    }
    
    public function new1Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $n1=$request->request->get('n1');
		$n2=$request->request->get('n2');
		$n3=$request->request->get('n3');
	    $tel=$request->request->get('tel');
		$dir=$request->request->get('dir');
		$email=$request->request->get('email');
		$rfc=$request->request->get('rfc');
		$sex=$request->request->get('sex');
        $pass=$request->request->get('pass');
        $nivel=$request->request->get('lvl');
        $noctrl=$request->request->get('noctrl');
		$ch = new profesores();
    	$ch->setNombre($n1);
    	$ch->setApellidoP($n2);
    	$ch->setApellidoM($n3);
    	$ch->setTelefono($tel);
    	$ch->setEmail($email);
        $ch->setRfc($rfc);
        $ch->setSexo($sex);
        $ch->setDireccion($dir);
        $ch->setStatus(1);
        $ch->setContrasena($pass);
        $ch->setNoCtrl($noctrl);
        $ch->setNivel($nivel);
        

    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();

        return $this->redirect($this->generateUrl('profesores'));
    }

    public function new2Action()
    {
       
    }


    /**
     * Finds and displays a profesores entity.
     *
     * @Route("/{id}", name="profesores_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:profesores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find profesores entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing profesores entity.
     *
     * @Route("/{id}/edit", name="profesores_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:profesores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find profesores entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a profesores entity.
    *
    * @param profesores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(profesores $entity)
    {
        $form = $this->createForm(new profesoresType(), $entity, array(
            'action' => $this->generateUrl('profesores_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing profesores entity.
     *
     * @Route("/{id}", name="profesores_update")
     * @Method("PUT")
     * @Template("proyectoejemploBundle:profesores:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:profesores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find profesores entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('profesores_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a profesores entity.
     *
     * @Route("/{id}", name="profesores_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('proyectoejemploBundle:profesores')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find profesores entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('profesores'));
    }

    /**
     * Creates a form to delete a profesores entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profesores_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
