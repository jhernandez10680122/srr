<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use proyecto\ejemploBundle\Entity\materias;
use proyecto\ejemploBundle\Form\materiasType;

/**
 * materias controller.
 *
 * @Route("/materias")
 */
class materiasController extends Controller
{

    /**
     * Lists all materias entities.
     *
     * @Route("/", name="materias")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('proyectoejemploBundle:materias')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new materias entity.
     *
     * @Route("/", name="materias_create")
     * @Method("POST")
     * @Template("proyectoejemploBundle:materias:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new materias();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('materias_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a materias entity.
    *
    * @param materias $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(materias $entity)
    {
        $form = $this->createForm(new materiasType(), $entity, array(
            'action' => $this->generateUrl('materias_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new materias entity.
     *
     * @Route("/new", name="materias_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new materias();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a materias entity.
     *
     * @Route("/{id}", name="materias_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:materias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find materias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing materias entity.
     *
     * @Route("/{id}/edit", name="materias_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:materias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find materias entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a materias entity.
    *
    * @param materias $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(materias $entity)
    {
        $form = $this->createForm(new materiasType(), $entity, array(
            'action' => $this->generateUrl('materias_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing materias entity.
     *
     * @Route("/{id}", name="materias_update")
     * @Method("PUT")
     * @Template("proyectoejemploBundle:materias:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:materias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find materias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('materias_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a materias entity.
     *
     * @Route("/{id}", name="materias_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('proyectoejemploBundle:materias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find materias entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('materias'));
    }

    /**
     * Creates a form to delete a materias entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('materias_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
