<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use proyecto\ejemploBundle\Entity\grupoAlumno;
use proyecto\ejemploBundle\Form\grupoAlumnoType;

/**
 * grupoAlumno controller.
 *
 * @Route("/grupoalumno")
 */
class grupoAlumnoController extends Controller
{

    /**
     * Lists all grupoAlumno entities.
     *
     * @Route("/", name="grupoalumno")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('proyectoejemploBundle:grupoAlumno')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new grupoAlumno entity.
     *
     * @Route("/", name="grupoalumno_create")
     * @Method("POST")
     * @Template("proyectoejemploBundle:grupoAlumno:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new grupoAlumno();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grupoAlumno_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a grupoAlumno entity.
    *
    * @param grupoAlumno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(grupoAlumno $entity)
    {
        $form = $this->createForm(new grupoAlumnoType(), $entity, array(
            'action' => $this->generateUrl('grupoalumno_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new grupoAlumno entity.
     *
     * @Route("/new", name="grupoalumno_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new grupoAlumno();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a grupoAlumno entity.
     *
     * @Route("/{id}", name="grupoalumno_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:grupoAlumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grupoAlumno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing grupoAlumno entity.
     *
     * @Route("/{id}/edit", name="grupoalumno_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:grupoAlumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grupoAlumno entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a grupoAlumno entity.
    *
    * @param grupoAlumno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(grupoAlumno $entity)
    {
        $form = $this->createForm(new grupoAlumnoType(), $entity, array(
            'action' => $this->generateUrl('grupoalumno_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing grupoAlumno entity.
     *
     * @Route("/{id}", name="grupoalumno_update")
     * @Method("PUT")
     * @Template("proyectoejemploBundle:grupoAlumno:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:grupoAlumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find grupoAlumno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grupoalumno_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a grupoAlumno entity.
     *
     * @Route("/{id}", name="grupoalumno_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('proyectoejemploBundle:grupoAlumno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find grupoAlumno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grupoalumno'));
    }

    /**
     * Creates a form to delete a grupoAlumno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grupoalumno_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
