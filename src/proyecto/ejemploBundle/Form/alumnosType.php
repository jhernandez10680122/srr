<?php

namespace proyecto\ejemploBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class alumnosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellidoP')
            ->add('apellidoM')
            ->add('direccion')
            ->add('noCtrl')
            ->add('semestre')
            ->add('email')
            ->add('carrera')
            ->add('ingreso')
            ->add('egreso')
            ->add('contrasena')
            ->add('sexo')
            ->add('status')
            ->add('reticula')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'proyecto\ejemploBundle\Entity\alumnos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proyecto_ejemplobundle_alumnos';
    }
}
