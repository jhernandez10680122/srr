<?php

namespace proyecto\ejemploBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class gruposType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave')
            ->add('status')
            ->add('periodo')
            ->add('anio')
            ->add('maxiAlu')
            ->add('idProfesor','entity',array(
            'class'=>'proyecto\ejemploBundle\Entity\profesores',
            'property'=>'noCtrl',
            'label'=>'noCtrl',
            'required'=>true
            ))

            ->add('idMateria','entity',array(
            'class'=>'proyecto\ejemploBundle\Entity\materias',
            'property'=>'nombre',
            'label'=>'nombre',
            'required'=>true
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'proyecto\ejemploBundle\Entity\grupos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proyecto_ejemplobundle_grupos';
    }
}
