<?php

namespace proyecto\ejemploBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class grupoAlumnoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('semestre')
            ->add('status')
            ->add('idAlumno','entity',array(
            'class'=>'proyecto\ejemploBundle\Entity\alumnos',
            'property'=>'noCtrl',
            'label'=>'noCtrl',
            'required'=>true
            ))

            ->add('idGrupo','entity',array(
            'class'=>'proyecto\ejemploBundle\Entity\grupos',
            'property'=>'clave',
            'label'=>'clave',
            'required'=>true
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'proyecto\ejemploBundle\Entity\grupoAlumno'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'proyecto_ejemplobundle_grupoalumno';
    }
}
