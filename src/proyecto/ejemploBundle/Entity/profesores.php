<?php

namespace proyecto\ejemploBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * profesores
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="proyecto\ejemploBundle\Entity\profesoresRepository")
 */
class profesores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoP", type="string", length=100)
     */
    private $apellidoP;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoM", type="string", length=100)
     */
    private $apellidoM;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;


    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;


    /**
     * @var string
     *
     * @ORM\Column(name="rfc", type="string", length=50)
     */
    private $rfc;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="contrasena", type="string", length=100)
     */
    private $contrasena;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=15)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="no_ctrl", type="string")
     */
    private $noCtrl;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nivel", type="integer")
     */
    private $nivel;

     /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
    * @ORM\OneToMany(targetEntity="grupos", mappedBy="profesores")
    */
    private $profesor;
    
    public function __construct(){
        $this->profesor = new \Doctrine\Common\Collections\ArrayCollection();
    }    

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return profesores
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidoP
     *
     * @param string $apellidoP
     * @return profesores
     */
    public function setApellidoP($apellidoP)
    {
        $this->apellidoP = $apellidoP;

        return $this;
    }

    /**
     * Get apellidoP
     *
     * @return string 
     */
    public function getApellidoP()
    {
        return $this->apellidoP;
    }

    /**
     * Set apellidoM
     *
     * @param string $apellidoM
     * @return profesores
     */
    public function setApellidoM($apellidoM)
    {
        $this->apellidoM = $apellidoM;

        return $this;
    }

    /**
     * Get apellidoM
     *
     * @return string 
     */
    public function getApellidoM()
    {
        return $this->apellidoM;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return profesores
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return profesores
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     * @return profesores
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string 
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return profesores
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set contrasena
     *
     * @param string $contrasena
     * @return profesores
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = md5 ($contrasena);

        return $this;
    }

    /**
     * Get contrasena
     *
     * @return string 
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     * @return profesores
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string 
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set noCtrl
     *
     * @param integer $noCtrl
     * @return profesores
     */
    public function setNoCtrl($noCtrl)
    {
        $this->noCtrl = $noCtrl;

        return $this;
    }

    /**
     * Get noCtrl
     *
     * @return integer 
     */
    public function getNoCtrl()
    {
        return $this->noCtrl;
    }

    /**
     * Set nivel
     *
     * @param integer $nivel
     * @return profesores
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return integer 
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return profesores
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add profesor
     *
     * @param \proyecto\ejemploBundle\Entity\grupos $profesor
     * @return profesores
     */
    public function addProfesor(\proyecto\ejemploBundle\Entity\grupos $profesor)
    {
        $this->profesor[] = $profesor;

        return $this;
    }

    /**
     * Remove profesor
     *
     * @param \proyecto\ejemploBundle\Entity\grupos $profesor
     */
    public function removeProfesor(\proyecto\ejemploBundle\Entity\grupos $profesor)
    {
        $this->profesor->removeElement($profesor);
    }

    /**
     * Get profesor
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfesor()
    {
        return $this->profesor;
    }
}
