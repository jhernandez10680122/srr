<?php

namespace proyecto\ejemploBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * grupoAlumno
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="proyecto\ejemploBundle\Entity\grupoAlumnoRepository")
 */
class grupoAlumno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="alumnos", inversedBy="alumno")
    * @ORM\JoinColumn(name="idAlumno",referencedColumnName="id")
    */
    private $idAlumno;
    
    
    /**
    * @ORM\ManyToOne(targetEntity="grupos", inversedBy="grupo")
    * @ORM\JoinColumn(name="idGrupo",referencedColumnName="id")
    */
    private $idGrupo;

    /**
     * @var integer
     *
     * @ORM\Column(name="semestre", type="integer")
     */
    private $semestre;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;
    
    /**
    * @ORM\OneToMany(targetEntity="calificaciones", mappedBy="grupoAlumno")
    */
    private $gp;
    
    public function __construct(){
        $this->gp = new \Doctrine\Common\Collections\ArrayCollection();
    }    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semestre
     *
     * @param integer $semestre
     * @return grupoAlumno
     */
    public function setSemestre($semestre)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre
     *
     * @return integer 
     */
    public function getSemestre()
    {
        return $this->semestre;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return grupoAlumno
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set idAlumno
     *
     * @param \proyecto\ejemploBundle\Entity\alumnos $idAlumno
     * @return grupoAlumno
     */
    public function setIdAlumno(\proyecto\ejemploBundle\Entity\alumnos $idAlumno = null)
    {
        $this->idAlumno = $idAlumno;

        return $this;
    }

    /**
     * Get idAlumno
     *
     * @return \proyecto\ejemploBundle\Entity\alumnos 
     */
    public function getIdAlumno()
    {
        return $this->idAlumno;
    }

    /**
     * Set idGrupo
     *
     * @param \proyecto\ejemploBundle\Entity\grupos $idGrupo
     * @return grupoAlumno
     */
    public function setIdGrupo(\proyecto\ejemploBundle\Entity\grupos $idGrupo = null)
    {
        $this->idGrupo = $idGrupo;

        return $this;
    }

    /**
     * Get idGrupo
     *
     * @return \proyecto\ejemploBundle\Entity\grupos 
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Add gp
     *
     * @param \proyecto\ejemploBundle\Entity\calificaciones $gp
     * @return grupoAlumno
     */
    public function addGp(\proyecto\ejemploBundle\Entity\calificaciones $gp)
    {
        $this->gp[] = $gp;

        return $this;
    }

    /**
     * Remove gp
     *
     * @param \proyecto\ejemploBundle\Entity\calificaciones $gp
     */
    public function removeGp(\proyecto\ejemploBundle\Entity\calificaciones $gp)
    {
        $this->gp->removeElement($gp);
    }

    /**
     * Get gp
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGp()
    {
        return $this->gp;
    }
}
