<?php

namespace proyecto\ejemploBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * materias
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="proyecto\ejemploBundle\Entity\materiasRepository")
 */
class materias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="reticula", type="string", length=50)
     */
    private $reticula;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return materias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return materias
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
     /**
     * Set reticula
     *
     * @param string $reticula
     * @return materias
     */
    public function setReticula($reticula)
    {
        $this->reticula = $reticula;

        return $this;
    }

    /**
     * Get reticula
     *
     * @return string 
     */
    public function getReticula()
    {
        return $this->reticula;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return materias
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
    * @ORM\OneToMany(targetEntity="grupos", mappedBy="materias")
    */
    private $materia;
    
    public function __construct(){
        $this->materia = new \Doctrine\Common\Collections\ArrayCollection();
    }    
   

    /**
     * Add materia
     *
     * @param \proyecto\ejemploBundle\Entity\grupos $materia
     * @return materias
     */
    public function addMaterium(\proyecto\ejemploBundle\Entity\grupos $materia)
    {
        $this->materia[] = $materia;

        return $this;
    }

    /**
     * Remove materia
     *
     * @param \proyecto\ejemploBundle\Entity\grupos $materia
     */
    public function removeMaterium(\proyecto\ejemploBundle\Entity\grupos $materia)
    {
        $this->materia->removeElement($materia);
    }

    /**
     * Get materia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMateria()
    {
        return $this->materia;
    }
}
