<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use proyecto\ejemploBundle\Entity\calificaciones;
use proyecto\ejemploBundle\Form\calificacionesType;

/**
 * calificaciones controller.
 *
 * @Route("/calificaciones")
 */
class calificacionesController extends Controller
{

    /**
     * Lists all calificaciones entities.
     *
     * @Route("/", name="calificaciones")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('proyectoejemploBundle:calificaciones')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new calificaciones entity.
     *
     * @Route("/", name="calificaciones_create")
     * @Method("POST")
     * @Template("proyectoejemploBundle:calificaciones:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new calificaciones();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('calificaciones_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a calificaciones entity.
    *
    * @param calificaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(calificaciones $entity)
    {
        $form = $this->createForm(new calificacionesType(), $entity, array(
            'action' => $this->generateUrl('calificaciones_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new calificaciones entity.
     *
     * @Route("/new", name="calificaciones_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new calificaciones();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a calificaciones entity.
     *
     * @Route("/{id}", name="calificaciones_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:calificaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find calificaciones entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing calificaciones entity.
     *
     * @Route("/{id}/edit", name="calificaciones_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:calificaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find calificaciones entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    


    public function icalAction( Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $ga = $em->getRepository('proyectoejemploBundle:grupoAlumno')->findOneById($id);
      return $this->render('proyectoejemploBundle:calificaciones:ical.html.twig', array( 'ga' => $ga));
    }
    
    
    public function daleAction( Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $grupo=$request->request->get('grupo');
        $u1=$request->request->get('u1');
        $u2=$request->request->get('u2');
        $u3=$request->request->get('u3');
        $u4=$request->request->get('u4');
        $u5=$request->request->get('u5');
        $u6=$request->request->get('u6');
        $u7=$request->request->get('u7');
		$u8=$request->request->get('u8');
		$u9=$request->request->get('u9');
	    $promedio=$request->request->get('promedio');
		$idgrupoAlumno=$request->request->get('idgrupoAlumno');
		$ga = $em->getRepository('proyectoejemploBundle:grupoAlumno')->findOneById($idgrupoAlumno);
		$ch = new calificaciones();
    	$ch->setU1($u1);
        $ch->setU2($u2);
        $ch->setU3($u3);
        $ch->setU4($u4);
        $ch->setU5($u5);
        $ch->setU6($u6);
        $ch->setU7($u7);
        $ch->setU8($u8);
        $ch->setU9($u9);
        $ch->setPromedio($promedio);
        $ch->setIdGp($ga);
    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();
        $entities = $em->getRepository('proyectoejemploBundle:grupoAlumno')->findAll();
		return $this->render('proyectoejemploBundle:calificaciones:sg3.html.twig', array(  'entities' => $entities,'grupo' => $grupo));
        
      
    }
    
    /**
    * Creates a form to edit a calificaciones entity.
    *
    * @param calificaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(calificaciones $entity)
    {
        $form = $this->createForm(new calificacionesType(), $entity, array(
            'action' => $this->generateUrl('calificaciones_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing calificaciones entity.
     *
     * @Route("/{id}", name="calificaciones_update")
     * @Method("PUT")
     * @Template("proyectoejemploBundle:calificaciones:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:calificaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find calificaciones entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('calificaciones_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    
    
    public function sg2Action(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
        $grupos = $em->getRepository('proyectoejemploBundle:grupos')->findAll();
		  $materia=$request->request->get('materia');
		return $this->render('proyectoejemploBundle:calificaciones:sg2.html.twig', array( 'materia' => $materia,'grupos' => $grupos));

    }
    
    
    public function sg3Action(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
       $grupo=$request->request->get('grupo');
       $entities = $em->getRepository('proyectoejemploBundle:grupoAlumno')->findAll();


		return $this->render('proyectoejemploBundle:calificaciones:sg3.html.twig', array(  'entities' => $entities,'grupo' => $grupo));

    }
    
    
    
    public function sg4Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $grupo=$request->request->get('grupo');
        $entities = $em->getRepository('proyectoejemploBundle:calificaciones')->findAll();
		return $this->render('proyectoejemploBundle:calificaciones:sg4.html.twig', array(  'entities' => $entities,'grupo' => $grupo));

    }
    
    
    public function sgAction(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
        $materias = $em->getRepository('proyectoejemploBundle:materias')->findAll();
		
		return $this->render('proyectoejemploBundle:calificaciones:sg.html.twig', array( 'materias' => $materias));

    }
    /**
     * Deletes a calificaciones entity.
     *
     * @Route("/{id}", name="calificaciones_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('proyectoejemploBundle:calificaciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find calificaciones entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('calificaciones'));
    }

    /**
     * Creates a form to delete a calificaciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('calificaciones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
