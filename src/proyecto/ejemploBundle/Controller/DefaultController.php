<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use proyecto\ejemploBundle\Entity\materias;
use proyecto\ejemploBundle\Entity\alumnos;
use proyecto\ejemploBundle\Entity\profesores;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('proyectoejemploBundle:Default:index.html.twig', array('name' => $name));
    
    }

    public function proyectoAction()
    {
        return $this->render('proyectoejemploBundle:Default:proyecto.html.twig');
    }
    
    
    public function tutorlAction(Request $request)
    {
        
        $request = $this->getRequest();
        $session = $request->getSession();
        $valor=$session->get('var');
        
        
        if($valor!=1){
        // obitene el objeto User de la session
        // $user = $this->get('security.context')->getToken()->getUser();
        // o  
        // $user = $this->getUser();
        // y se leen de esta forma los datos
        // $var = $user->getUsername();
        // en twig
        // <p>Username: {{ app.user.username }}</p>
        // link
        // http://gitnacho.github.io/symfony-docs-es/book/
        //    security.html
        // http://gitnacho.github.io/symfony-docs-es/cookbook/security/entity_provider.html
        //    

        // obtiene el error de inicio de sesión si lo hay
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        

        return $this->render(
            'proyectoejemploBundle:Default:tutorl.html.twig',
            array('error'=> $error)
        );
        }
       else {
        return $this->render('proyectoejemploBundle:Default:proyecto.html.twig');
        }
    }


    public function insertarAction()
    {

        return $this->render('proyectoejemploBundle:Default:insertar.html.twig');
    }
    

    
    
        
        
    public function vergruposAction()
    {
         $em = $this->getDoctrine()->getManager();
        $grupos = $em->getRepository('proyectoejemploBundle:grupos')->findAll();
        return $this->render('proyectoejemploBundle:Default:vergrupos.html.twig', array( 'grupos' => $grupos));

    }
    
    
    public function login_checkAction(Request $request)
    {
        
        //$em = $this->getDoctrine()->getManager();
        //$user= new User();
        //$query = $em->createQuery('UPDATE ingluis\ittBundle\Entity\User u set u.isActive=0 WHERE u.username=?1 and u.password=?2');
        //$usu=$request->request->get('usu');
        //$pass=$request->request->get('pass');
        //$query->setParameter(1, $usu);
        //$query->setParameter(2, md5($pass));
        //$users = $query->getResult();
        
        $session = $request->getSession();
        $valor=$session->get('var');
        
        if($valor!=1){
        $usu=$request->request->get('usu');
        $pass=$request->request->get('pass');
        
        $repository = $this->getDoctrine()->getRepository('proyectoejemploBundle:profesores');
        $query = $repository->createQueryBuilder('u')
        ->where('u.rfc = :usu AND u.contrasena = :pass AND u.status = 1')
        ->setParameter('usu', $usu)
        ->setParameter('pass', md5($pass))
        ->orderBy('u.id', 'ASC')
        ->getQuery();
 
        $user = $query->getResult();
        $valor = 0;
        $lvl = 3;
        foreach ($user as $v1) {
            
            $lvl = $v1 ->getNivel();
             if($lvl==1 or $lvl==2){
            $valor = 1;
            $session->set('userses', $v1);
            $session->set('var', $valor);
			$session->set('whoami', $v1->getID());
            $session->set('lvl', $v1->getNivel());
            $session->set('nom_ses', $v1->getNombre());   
			$session->set('nom_ap', $v1->getApellidoP());   
			$session->set('nom_am', $v1->getApellidoM());  
            }
        }

        
        if($lvl==1 or $lvl==2){
            $var_ses=$session->get('var');
            return $this->render('proyectoejemploBundle:Default:proyecto.html.twig');
        }
        else{
            $error="USUARIO O CONTRASEÑA INCORRECTOS";
            return $this->render('proyectoejemploBundle:Default:tutorl.html.twig', array('error' => $error));
        }
             }
       else {
        return $this->render('proyectoejemploBundle:Default:proyecto.html.twig');
        }
    }
    

    
    public function exitAction(Request $request)
    {
		
		$session = $request->getSession();
		$ko = '';
		$session->set('userses', $ko);
		$session->set('whoami', $ko);
        $session->set('lvl', $ko);
		$session->set('nom_ses', $ko);
		$session->set('nom_ap', $ko);
		$session->set('nom_am', $ko);
	
		$valor=$session->get('var');
	
	if($valor==1)
	{$session->set('var', $ko);
		return $this->render('proyectoejemploBundle:Default:proyecto.html.twig');
	}

    }


     public function insertarmatAction()
    {
    	$materia = new materias();
    	$materia ->setNombre("Calculo");
    	$materia ->setDescripcion("");
    	$materia ->setStatus(1);
    	$materia ->setReticula(2010);

    	$entman= $this->getDoctrine()->getManager();
    	$entman->persist($materia);
    	$entman->flush();
        return $this->redirect($this->generateUrl('proyectoejemplo_consultasmat')); 
    }

      public function insertaraluAction()
    {
    	$alumno = new alumnos();
    	$alumno ->setNombre("Martha");
    	$alumno ->setApellidoP("Morales");
    	$alumno ->setApellidoM("Santamaria");
    	$alumno ->setDireccion("tonantzon 13 rancho de maya");
    	$alumno ->setNoCtrl(10280570);
    	$alumno ->setSemestre(8);
    	$alumno ->setIdCarrera(1);
    	$alumno ->setEmail("jjazme@gmail.com");
    	$alumno ->setIngreso("junio-dic 2010");
        $alumno ->setEgreso("");
        $alumno ->setContrasena("pw7677");
    	$alumno ->setSexo("F");
    	$alumno ->setStatus(1);
    	$alumno ->setReticula(2010);
    	

    	$entman= $this->getDoctrine()->getManager();
    	$entman->persist($alumno);
    	$entman->flush();
        return $this->redirect($this->generateUrl('proyectoejemplo_consultasalu')); 
    }

      public function insertarprofAction()
    {
    	$profesor = new profesores();
    	$profesor ->setNombre("Luis");
    	$profesor ->setApellidoP("Estrada");
    	$profesor ->setApellidoM("Manuel");
    	$profesor ->setDireccion("centro");
        $profesor ->setTelefono(211233474);
    	$profesor ->setRFC("ESML254585MMMSKCKS");
    	$profesor ->setEmail("AKSJKNJ@xxcn .ksa");
        $profesor ->setContrasena("pw1234");
        $profesor ->setNoCtrl(25874);
    	$profesor ->setSexo("M");
    	$profesor ->setStatus(1);
    	
    	$entman= $this->getDoctrine()->getManager();
    	$entman->persist($profesor);
    	$entman->flush();
        return $this->redirect($this->generateUrl('proyectoejemplo_consultasprof')); 
    }


    public function consultasAction()
    {
        return $this->render('proyectoejemploBundle:Default:consultas.html.twig');
    }

    public function consultasmatAction()
    {
    	$repository = $this ->getDoctrine()->getRepository('proyectoejemploBundle:materias');
    	$materias = $repository->findAll();
        return $this->render('proyectoejemploBundle:Default:consultasmat.html.twig',array('mate'=>$materias));
    }

    public function consultasaluAction()
    {
    	$repository = $this ->getDoctrine()->getRepository('proyectoejemploBundle:alumnos');
    	$alumnos = $repository->findAll();	
        return $this->render('proyectoejemploBundle:Default:consultasalu.html.twig',array('alu'=>$alumnos));
    }

    public function consultasprofAction()
    {
    	$repository = $this ->getDoctrine()->getRepository('proyectoejemploBundle:profesores');
    	$profesores = $repository->findAll();
        return $this->render('proyectoejemploBundle:Default:consultasprof.html.twig',array('profe'=>$profesores));
    }
}
