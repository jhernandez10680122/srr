<?php

namespace proyecto\ejemploBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use proyecto\ejemploBundle\Entity\alumnos;
use proyecto\ejemploBundle\Form\alumnosType;

/**
 * alumnos controller.
 *
 * @Route("/alumnos")
 */
class alumnosController extends Controller
{

    /**
     * Lists all alumnos entities.
     *
     * @Route("/", name="alumnos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('proyectoejemploBundle:alumnos')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new alumnos entity.
     *
     * @Route("/", name="alumnos_create")
     * @Method("POST")
     * @Template("proyectoejemploBundle:alumnos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new alumnos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alumnos_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a alumnos entity.
    *
    * @param alumnos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(alumnos $entity)
    {
        $form = $this->createForm(new alumnosType(), $entity, array(
            'action' => $this->generateUrl('alumnos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new alumnos entity.
     *
     * @Route("/new", name="alumnos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new alumnos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a alumnos entity.
     *
     * @Route("/{id}", name="alumnos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:alumnos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find alumnos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing alumnos entity.
     *
     * @Route("/{id}/edit", name="alumnos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:alumnos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find alumnos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a alumnos entity.
    *
    * @param alumnos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(alumnos $entity)
    {
        $form = $this->createForm(new alumnosType(), $entity, array(
            'action' => $this->generateUrl('alumnos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing alumnos entity.
     *
     * @Route("/{id}", name="alumnos_update")
     * @Method("PUT")
     * @Template("proyectoejemploBundle:alumnos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('proyectoejemploBundle:alumnos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find alumnos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('alumnos_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a alumnos entity.
     *
     * @Route("/{id}", name="alumnos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('proyectoejemploBundle:alumnos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find alumnos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alumnos'));
    }

    /**
     * Creates a form to delete a alumnos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('alumnos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
