<?php

namespace XPS\SSRBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HistorialType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('NoOficioLiberacion')
            ->add('FechaLiberacion')
            ->add('Calificacion')
            ->add('Estado')
            ->add('IdResidencia', 'entity', array('class' => 'XPSSSRBundle:Residencia','property' => 'id',))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'XPS\SSRBundle\Entity\Historial'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'xps_ssrbundle_historial';
    }
}
