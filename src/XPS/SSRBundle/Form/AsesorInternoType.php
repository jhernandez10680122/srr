<?php

namespace XPS\SSRBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AsesorInternoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ApPaterno')
            ->add('ApMaterno')
            ->add('Nombre')
            ->add('Especialidad')
            ->add('Telefono')
            ->add('Estado')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'XPS\SSRBundle\Entity\AsesorInterno'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'xps_ssrbundle_asesorinterno';
    }
}
