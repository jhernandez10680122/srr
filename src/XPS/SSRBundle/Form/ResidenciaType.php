<?php

namespace XPS\SSRBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResidenciaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('IdResidencia')
            ->add('RecursosHumanos')
            ->add('NoOficialAsesorInterno')
            ->add('AsesorExterno')
            ->add('NombreDeProyecto')
            ->add('FechaDictamen')
            ->add('FechaRevision1')
            ->add('FechaRevision2')
            ->add('FechaRevision3')
            ->add('Observaciones')
            ->add('Estado')         
            ->add('NoDeControl', 'entity', array('class' => 'XPSSSRBundle:Alumno','property' => 'id',))
            ->add('IdEmpresa', 'entity', array('class' => 'XPSSSRBundle:Empresa','property' => 'nombre',))
            ->add('IdAsesorInterno', 'entity', array('class' => 'XPSSSRBundle:AsesorInterno','property' => 'id',))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'XPS\SSRBundle\Entity\Residencia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'xps_ssrbundle_residencia';
    }
}
