<?php

namespace XPS\SSRBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use XPS\SSRBundle\Entity\Historial;
use XPS\SSRBundle\Form\HistorialType;

/**
 * Historial controller.
 *
 */
class HistorialController extends Controller
{

    /**
     * Lists all Historial entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:Historial')->findAll();

        return $this->render('XPSSSRBundle:Historial:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Historial entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Historial();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('historial_show', array('id' => $entity->getId())));
        }

        return $this->render('XPSSSRBundle:Historial:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Historial entity.
     *
     * @param Historial $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Historial $entity)
    {
        $form = $this->createForm(new HistorialType(), $entity, array(
            'action' => $this->generateUrl('historial_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Historial entity.
     *
     */
    public function newAction()
    {   $em = $this->getDoctrine()->getManager();
       $residencias = $em->getRepository('XPSSSRBundle:Residencia')->findAll();
        return $this->render('XPSSSRBundle:Historial:new.html.twig', array(
            'residencias'      => $residencias,
        ));
    }

    /**
     * Finds and displays a Historial entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:Historial:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Historial entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);
        $residencias = $em->getRepository('XPSSSRBundle:Residencia')->findAll();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:Historial:edit.html.twig', array(
            'entity'      => $entity,
            'residencias'      => $residencias,
        ));
    }

    /**
    * Creates a form to edit a Historial entity.
    *
    * @param Historial $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Historial $entity)
    {
        $form = $this->createForm(new HistorialType(), $entity, array(
            'action' => $this->generateUrl('historial_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Historial entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('historial_edit', array('id' => $id)));
        }

        return $this->render('XPSSSRBundle:Historial:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Historial entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Historial entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('historial'));
    }

    /**
     * Creates a form to delete a Historial entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('historial_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    
public function disaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);
        $entity->setEstado(0);
        $em->flush();
            
       $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }


        return $this->render('XPSSSRBundle:Historial:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
        public function nvoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $NoLib=$request->request->get('NoLib');
        $FechaLib=$request->request->get('FechaLib');
        $Cal=$request->request->get('Cal');
        $RID=$request->request->get('RID');
		$ch = new Historial();
        $ch->setNoOficioLiberacion($NoLib);
        $ch->setFechaLiberacion(new \DateTime($FechaLib));
        $ch->setCalificacion($Cal);
        $ch->setIdResidencia($em->getRepository('XPSSSRBundle:Residencia')->find($RID));
        $ch->setEstado(1);
    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();
        $id = $ch->getId();
       $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }


        return $this->render('XPSSSRBundle:Historial:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
    
    public function editaAction(Request $request)
    {
            
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $ch = $em->getRepository('XPSSSRBundle:Historial')->find($id);

        if (!$ch) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }
            
            $em = $this->getDoctrine()->getManager();
            $NoLib=$request->request->get('Nolib');
            $FechaLib=$request->request->get('FechaLib');
            $Cal=$request->request->get('Cal');
            $RID=$request->request->get('RID');
            $estado=$request->request->get('estado');
            $ch->setEstado($estado);
            $ch->setNoOficioLiberacion($NoLib);
            $ch->setFechaLiberacion(new \DateTime($FechaLib));
            $ch->setCalificacion($Cal);
            $ch->setIdResidencia($em->getRepository('XPSSSRBundle:Residencia')->find($RID));

            $em->flush();
            
            
        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }
        $residencias = $em->getRepository('XPSSSRBundle:Residencia')->findAll();

        return $this->render('XPSSSRBundle:Historial:edit.html.twig', array(
            'entity'      => $entity,
            'residencias'      => $residencias,
        ));
    }
    

}
