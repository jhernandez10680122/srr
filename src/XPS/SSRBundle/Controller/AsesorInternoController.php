<?php

namespace XPS\SSRBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use XPS\SSRBundle\Entity\AsesorInterno;
use XPS\SSRBundle\Form\AsesorInternoType;

/**
 * AsesorInterno controller.
 *
 */
class AsesorInternoController extends Controller
{

    /**
     * Lists all AsesorInterno entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:AsesorInterno')->findAll();

        return $this->render('XPSSSRBundle:AsesorInterno:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new AsesorInterno entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new AsesorInterno();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('asesorinterno_show', array('id' => $entity->getId())));
        }

        return $this->render('XPSSSRBundle:AsesorInterno:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a AsesorInterno entity.
     *
     * @param AsesorInterno $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AsesorInterno $entity)
    {
        $form = $this->createForm(new AsesorInternoType(), $entity, array(
            'action' => $this->generateUrl('asesorinterno_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AsesorInterno entity.
     *
     */
    public function newAction()
    {
        $entity = new AsesorInterno();
        $form   = $this->createCreateForm($entity);

        return $this->render('XPSSSRBundle:AsesorInterno:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a AsesorInterno entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:AsesorInterno:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AsesorInterno entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:AsesorInterno:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a AsesorInterno entity.
    *
    * @param AsesorInterno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AsesorInterno $entity)
    {
        $form = $this->createForm(new AsesorInternoType(), $entity, array(
            'action' => $this->generateUrl('asesorinterno_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing AsesorInterno entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('asesorinterno_edit', array('id' => $id)));
        }

        return $this->render('XPSSSRBundle:AsesorInterno:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a AsesorInterno entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('asesorinterno'));
    }

    /**
     * Creates a form to delete a AsesorInterno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('asesorinterno_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
        public function nvoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $n1=$request->request->get('n1');
		$n2=$request->request->get('n2');
		$n3=$request->request->get('n3');
        $tel=$request->request->get('tel');
        $esp=$request->request->get('esp');
		$ch = new AsesorInterno();
    	$ch->setNombre($n1);
    	$ch->setApPaterno($n2);
    	$ch->setApMaterno($n3);
    	$ch->setTelefono($tel);
        $ch->setEspecialidad($esp);
        $ch->setEstado(1);
    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();
        $id = $ch->getId();
       $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }


        return $this->render('XPSSSRBundle:AsesorInterno:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
    
    public function editaAction(Request $request)
    {
            
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $ch = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$ch) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }
            
        $n1=$request->request->get('n1');
		$n2=$request->request->get('n2');
		$n3=$request->request->get('n3');
        $tel=$request->request->get('tel');
        $esp=$request->request->get('esp');
        $estado=$request->request->get('estado');
    	$ch->setNombre($n1);
    	$ch->setApPaterno($n2);
    	$ch->setApMaterno($n3);
    	$ch->setTelefono($tel);
        $ch->setEspecialidad($esp);
        $ch->setEstado($estado);

            
            $em->flush();
            
            
        $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }


        return $this->render('XPSSSRBundle:AsesorInterno:edit.html.twig', array(
            'entity'      => $entity
        ));
    }
    

            public function disaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);
        $entity->setEstado(0);
        $em->flush();
            
       $entity = $em->getRepository('XPSSSRBundle:AsesorInterno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsesorInterno entity.');
        }


        return $this->render('XPSSSRBundle:AsesorInterno:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
}
