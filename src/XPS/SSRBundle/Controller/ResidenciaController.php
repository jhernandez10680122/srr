<?php

namespace XPS\SSRBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use XPS\SSRBundle\Entity\Residencia;
use XPS\SSRBundle\Form\ResidenciaType;

/**
 * Residencia controller.
 *
 */
class ResidenciaController extends Controller
{

    /**
     * Lists all Residencia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:Residencia')->findAll();

        return $this->render('XPSSSRBundle:Residencia:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Residencia entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Residencia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('residencia_show', array('id' => $entity->getId())));
        }

        return $this->render('XPSSSRBundle:Residencia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Residencia entity.
     *
     * @param Residencia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Residencia $entity)
    {
        $form = $this->createForm(new ResidenciaType(), $entity, array(
            'action' => $this->generateUrl('residencia_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Residencia entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $alumnos = $em->getRepository('XPSSSRBundle:Alumno')->findAll();
        $asesores = $em->getRepository('XPSSSRBundle:AsesorInterno')->findAll();
        $empresas = $em->getRepository('XPSSSRBundle:Empresa')->findAll();
        

        return $this->render('XPSSSRBundle:Residencia:new.html.twig', array(
           'alumnos' => $alumnos,
            'empresas' => $empresas,
            'asesores' => $asesores,
        ));
    }

    /**
     * Finds and displays a Residencia entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:Residencia:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Residencia entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }
        $alumnos = $em->getRepository('XPSSSRBundle:Alumno')->findAll();
        $asesores = $em->getRepository('XPSSSRBundle:AsesorInterno')->findAll();
        $empresas = $em->getRepository('XPSSSRBundle:Empresa')->findAll();
        return $this->render('XPSSSRBundle:Residencia:edit.html.twig', array(
            'entity'      => $entity,
            'alumnos' => $alumnos,
            'empresas' => $empresas,
            'asesores' => $asesores,
        ));
    }

    /**
    * Creates a form to edit a Residencia entity.
    *
    * @param Residencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Residencia $entity)
    {
        $form = $this->createForm(new ResidenciaType(), $entity, array(
            'action' => $this->generateUrl('residencia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Residencia entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('residencia_edit', array('id' => $id)));
        }

        return $this->render('XPSSSRBundle:Residencia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Residencia entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Residencia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('residencia'));
    }

    /**
     * Creates a form to delete a Residencia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('residencia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    
                public function disaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);
        $entity->setEstado(0);
        $em->flush();
            
       $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }


        return $this->render('XPSSSRBundle:Residencia:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
    
        public function nvoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $noctl=$request->request->get('noctl');
        $aid=$request->request->get('aid');
        $emp=$request->request->get('emp');
        $idr=$request->request->get('idr');
        $noRH=$request->request->get('noRH');
        $noAsesor=$request->request->get('noAsesor');
        $asesorE=$request->request->get('asesorE');
        $Proyecto=$request->request->get('Proyecto');
        $FD=$request->request->get('FD');
        $F1=$request->request->get('F1');
        $F2=$request->request->get('F2');
        $F3=$request->request->get('F3');
        $obs=$request->request->get('obs');
		$ch = new Residencia();
        $ch->setIdResidencia($idr);
        $ch->setRecursosHumanos($noRH);
        $ch->setNoOficialAsesorInterno($noAsesor);
        $ch->setAsesorExterno($asesorE);
        $ch->setNombreDeProyecto($Proyecto);
        $ch->setFechaDictamen(new \DateTime($FD));
        $ch->setFechaRevision1(new \DateTime($F1));
        $ch->setFechaRevision2(new \DateTime($F2));
        $ch->setFechaRevision3(new \DateTime($F3));
        $ch->setObservaciones($obs);
        $ch->setNoDeControl($em->getRepository('XPSSSRBundle:Alumno')->find($noctl));
        $ch->setIdAsesorInterno($em->getRepository('XPSSSRBundle:AsesorInterno')->find($aid));
        $ch->setIdEmpresa($em->getRepository('XPSSSRBundle:Empresa')->find($emp));
        $ch->setEstado(1);
    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();
        $id = $ch->getId();
        
       $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }


        return $this->render('XPSSSRBundle:Residencia:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
     public function editaAction(Request $request)
    {
            
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $ch = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$ch) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }
                 
         
        $em = $this->getDoctrine()->getManager();
        $estado=$request->request->get('estado');
        $noctl=$request->request->get('noctl');
        $aid=$request->request->get('aid');
        $emp=$request->request->get('emp');
        $idr=$request->request->get('idr');
        $noRH=$request->request->get('noRH');
        $noAsesor=$request->request->get('noAsesor');
        $asesorE=$request->request->get('asesorE');
        $Proyecto=$request->request->get('Proyecto');
        $FD=$request->request->get('FD');
        $F1=$request->request->get('F1');
        $F2=$request->request->get('F2');
        $F3=$request->request->get('F3');
        $obs=$request->request->get('obs');
        $ch->setIdResidencia($idr);
        $ch->setRecursosHumanos($noRH);
        $ch->setNoOficialAsesorInterno($noAsesor);
        $ch->setAsesorExterno($asesorE);
        $ch->setNombreDeProyecto($Proyecto);
        $ch->setFechaDictamen(new \DateTime($FD));
        $ch->setFechaRevision1(new \DateTime($F1));
        $ch->setFechaRevision2(new \DateTime($F2));
        $ch->setFechaRevision3(new \DateTime($F3));
        $ch->setObservaciones($obs);
        $ch->setNoDeControl($em->getRepository('XPSSSRBundle:Alumno')->find($noctl));
        $ch->setIdAsesorInterno($em->getRepository('XPSSSRBundle:AsesorInterno')->find($aid));
        $ch->setIdEmpresa($em->getRepository('XPSSSRBundle:Empresa')->find($emp));
        $ch->setEstado($estado);
        $em->flush();
            
            
        $entity = $em->getRepository('XPSSSRBundle:Residencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Residencia entity.');
        }


        $alumnos = $em->getRepository('XPSSSRBundle:Alumno')->findAll();
        $asesores = $em->getRepository('XPSSSRBundle:AsesorInterno')->findAll();
        $empresas = $em->getRepository('XPSSSRBundle:Empresa')->findAll();
        return $this->render('XPSSSRBundle:Residencia:edit.html.twig', array(
            'entity'      => $entity,
            'alumnos' => $alumnos,
            'empresas' => $empresas,
            'asesores' => $asesores,
        ));
    }
}
