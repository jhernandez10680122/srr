<?php

namespace XPS\SSRBundle\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:Historial')->findAll();

        return $this->render('XPSSSRBundle:Default:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    
      public function CartaLAction(Request $request)
    {          
        $id=$request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);
          
        $html = $this->renderView('XPSSSRBundle:Docs:CartaL.html.twig', array(
            'entity'      => $entity,
        ));
        
        return new Response(
        $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
        200,
        array(
        'Content-Type'        => 'application/pdf',
        'Content-Disposition' => 'attachment; filename="fichero.pdf"'
        )
        );
          

      }
    
    
  public function DictamenAction( )
    {          
        return $this->render('XPSSSRBundle:Docs:new.html.twig');
      }
    
    
    public function AsesorAction(Request $request)
    {
              
        $id=$request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('XPSSSRBundle:Historial')->find($id);
        $html = $this->renderView('XPSSSRBundle:Docs:Asesor.html.twig', array(
            'entity'      => $entity,
        ));
        return new Response(
        $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
        200,
        array(
        'Content-Type'        => 'application/pdf',
        'Content-Disposition' => 'attachment; filename="fichero.pdf"'
        )
        );
        
        
          

      }
    

        public function Dictamen2Action(Request $request)
    {


    $per1=$request->request->get('per1');
        $per2=$request->request->get('per2');
        $periodo = "$per1" . "$per2";
        $error = "ninguno";
            
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:Historial')->findAll();
        
            
        return $this->render('XPSSSRBundle:Docs:new2.html.twig', array(
            'entities' => $entities,
            'periodo'   => $periodo,
            'per1'   => $per1,
            'per2'   => $per2,
        ));


      }

    
    
        public function Dictamen3Action(Request $request)
    {


        $start=$request->request->get('start');
        $end=$request->request->get('end');
        $nd=$request->request->get('nd');
            $per1=$request->request->get('per1');
        $per2=$request->request->get('per2');

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('XPSSSRBundle:Historial')->findAll();
        
$html = $this->renderView('XPSSSRBundle:Docs:Dictamen.html.twig', array(
            'entities' => $entities,
            'start'   => $start,
            'end'   => $end,
            'nd'   => $nd,
            'per1'   => $per1,
            'per2'   => $per2,
        ));
        return new Response(
        $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
        array('orientation'=>'Landscape')),
        200, 
        array(
        'Content-Type' => 'application/pdf', 
        'Content-Disposition' => 'attachment; filename="fichero.pdf"'
        )
        );

      }


}
