<?php

namespace XPS\SSRBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use XPS\SSRBundle\Entity\Empresa;
use XPS\SSRBundle\Form\EmpresaType;

/**
 * Empresa controller.
 *
 */
class EmpresaController extends Controller
{

    /**
     * Lists all Empresa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:Empresa')->findAll();

        return $this->render('XPSSSRBundle:Empresa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Empresa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Empresa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('empresa_show', array('id' => $entity->getId())));
        }

        return $this->render('XPSSSRBundle:Empresa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Empresa entity.
     *
     * @param Empresa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Empresa $entity)
    {
        $form = $this->createForm(new EmpresaType(), $entity, array(
            'action' => $this->generateUrl('empresa_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Empresa entity.
     *
     */
    public function newAction()
    {
        $entity = new Empresa();
        $form   = $this->createCreateForm($entity);

        return $this->render('XPSSSRBundle:Empresa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Empresa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:Empresa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Empresa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:Empresa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Empresa entity.
    *
    * @param Empresa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Empresa $entity)
    {
        $form = $this->createForm(new EmpresaType(), $entity, array(
            'action' => $this->generateUrl('empresa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Empresa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('empresa_edit', array('id' => $id)));
        }

        return $this->render('XPSSSRBundle:Empresa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Empresa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Empresa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('empresa'));
    }

    /**
     * Creates a form to delete a Empresa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('empresa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
                        public function disaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);
        $entity->setEstado(0);
        $em->flush();
            
       $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }


        return $this->render('XPSSSRBundle:Empresa:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
    
    public function nvoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $n1=$request->request->get('n1');
        $calle=$request->request->get('calle');
        $local=$request->request->get('local');
        $col=$request->request->get('col');
        $cp=$request->request->get('cp');
        $num1=$request->request->get('num1');
        $email=$request->request->get('email');
		$ch = new Empresa();
    	$ch->setNombre($n1);
        $ch->setCalle($calle);
        $ch->setLocalidad($local);
        $ch->setColonia($col);
        $ch->setCP($cp);
        $ch->setTelefono($esp);
        $ch->setCorreo($email);
        $ch->setEstado(1);
    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();
        $id = $ch->getId();
       $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }


        return $this->render('XPSSSRBundle:Empresa:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
    
    public function editaAction(Request $request)
    {
            
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $ch = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$ch) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }
            
        $n1=$request->request->get('n1');
        $calle=$request->request->get('calle');
        $local=$request->request->get('local');
        $col=$request->request->get('col');
        $cp=$request->request->get('cp');
        $num1=$request->request->get('num1');
        $email=$request->request->get('email');
        $estado=$request->request->get('estado');
    	$ch->setNombre($n1);
        $ch->setCalle($calle);
        $ch->setLocalidad($local);
        $ch->setColonia($col);
        $ch->setCP($cp);
        $ch->setTelefono($num1);
        $ch->setCorreo($email);
        $ch->setEstado($estado);
            
        $em->flush();
            
            
        $entity = $em->getRepository('XPSSSRBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }


        return $this->render('XPSSSRBundle:Empresa:edit.html.twig', array(
            'entity'      => $entity
        ));
    }
}
