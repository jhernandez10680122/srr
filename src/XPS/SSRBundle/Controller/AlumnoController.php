<?php

namespace XPS\SSRBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use XPS\SSRBundle\Entity\Alumno;
use XPS\SSRBundle\Form\AlumnoType;

/**
 * Alumno controller.
 *
 */
class AlumnoController extends Controller
{

    /**
     * Lists all Alumno entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('XPSSSRBundle:Alumno')->findAll();

        return $this->render('XPSSSRBundle:Alumno:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Alumno entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Alumno();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alumno_show', array('id' => $entity->getId())));
        }

        return $this->render('XPSSSRBundle:Alumno:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Alumno entity.
     *
     * @param Alumno $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Alumno $entity)
    {
        $form = $this->createForm(new AlumnoType(), $entity, array(
            'action' => $this->generateUrl('alumno_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Alumno entity.
     *
     */
    public function newAction()
    {
        $entity = new Alumno();
        $form   = $this->createCreateForm($entity);

        return $this->render('XPSSSRBundle:Alumno:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    
    
    

    public function nvoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $n1=$request->request->get('n1');
		$n2=$request->request->get('n2');
		$n3=$request->request->get('n3');
        $sexo=$request->request->get('sexo');
		$email=$request->request->get('email');
        $noctrl=$request->request->get('noctrl');
        $tel=$request->request->get('tel');
        $per1=$request->request->get('per1');
        $per2=$request->request->get('per2');
		$ch = new Alumno();
    	$ch->setNombre($n1);
    	$ch->setApPaterno($n2);
    	$ch->setApMaterno($n3);
    	$ch->setTelefono($tel);
    	$ch->setCorreo($email);
        $ch->setSexo($sexo);
        $ch->setEstado(1);
        $ch->setId($noctrl);
        $ch->setPeriodo("$per1" . "$per2");
    	$entman  = $this->getDoctrine()->getManager();
    	$entman->persist($ch);
    	$entman->flush();

        
       $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($noctrl);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }


        return $this->render('XPSSSRBundle:Alumno:show.html.twig', array(
            'entity'      => $entity,
        ));
    }

    /**
     * Finds and displays a Alumno entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('XPSSSRBundle:Alumno:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Alumno entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }


        return $this->render('XPSSSRBundle:Alumno:edit.html.twig', array(
            'entity'      => $entity
        ));
    }
    
    
        public function disaAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($id);
        $entity->setEstado(0);
        $em->flush();
            
       $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }


        return $this->render('XPSSSRBundle:Alumno:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
    
    

    /**
    * Creates a form to edit a Alumno entity.
    *
    * @param Alumno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Alumno $entity)
    {
        $form = $this->createForm(new AlumnoType(), $entity, array(
            'action' => $this->generateUrl('alumno_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Alumno entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('alumno_edit', array('id' => $id)));
        }

        return $this->render('XPSSSRBundle:Alumno:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    
    
        public function editaAction(Request $request)
    {
            
        $id=$request->request->get('noctrl_v');
        $em = $this->getDoctrine()->getManager();
        $ch = $em->getRepository('XPSSSRBundle:Alumno')->find($id);

        if (!$ch) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }
            
            $n1=$request->request->get('n1');
            $n2=$request->request->get('n2');
            $n3=$request->request->get('n3');
            $sexo=$request->request->get('sexo');
            $email=$request->request->get('email');
            $noctrl=$request->request->get('noctrl');
            $estado=$request->request->get('estado');
            $tel=$request->request->get('tel');
            $per1=$request->request->get('per1');
            $per2=$request->request->get('per2');
            $ch->setNombre($n1);
            $ch->setApPaterno($n2);
            $ch->setApMaterno($n3);
            $ch->setTelefono($tel);
            $ch->setCorreo($email);
            $ch->setSexo($sexo);
            $ch->setEstado($estado);
            $ch->setId($noctrl);
            $ch->setPeriodo("$per1" . "$per2");

            
            $em->flush();
            
            
        $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($noctrl);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }


        return $this->render('XPSSSRBundle:Alumno:edit.html.twig', array(
            'entity'      => $entity
        ));
 
    }
    /**
     * Deletes a Alumno entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('XPSSSRBundle:Alumno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Alumno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alumno'));
    }

    /**
     * Creates a form to delete a Alumno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('alumno_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
