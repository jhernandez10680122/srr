<?php

namespace XPS\SSRBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historial
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="XPS\SSRBundle\Entity\HistorialRepository")
 */
class Historial
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NoOficioLiberacion", type="string", length=255)
     */
    private $NoOficioLiberacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaLiberacion", type="date")
     */
    private $FechaLiberacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="Calificacion", type="integer")
     */
    private $Calificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Estado", type="boolean")
     */
    private $Estado;
    
    
    /**
     * @ORM\OneToOne(targetEntity="Residencia")
     * @ORM\JoinColumn(name="IdResidencia", referencedColumnName="id")
     **/
    private $IdResidencia;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set NoOficioLiberacion
     *
     * @param string $noOficioLiberacion
     * @return Historial
     */
    public function setNoOficioLiberacion($noOficioLiberacion)
    {
        $this->NoOficioLiberacion = $noOficioLiberacion;

        return $this;
    }

    /**
     * Get NoOficioLiberacion
     *
     * @return string 
     */
    public function getNoOficioLiberacion()
    {
        return $this->NoOficioLiberacion;
    }

    /**
     * Set FechaLiberacion
     *
     * @param \DateTime $fechaLiberacion
     * @return Historial
     */
    public function setFechaLiberacion($fechaLiberacion)
    {
        $this->FechaLiberacion = $fechaLiberacion;

        return $this;
    }

    /**
     * Get FechaLiberacion
     *
     * @return \DateTime 
     */
    public function getFechaLiberacion()
    {
        return $this->FechaLiberacion;
    }

    /**
     * Set Calificacion
     *
     * @param integer $calificacion
     * @return Historial
     */
    public function setCalificacion($calificacion)
    {
        $this->Calificacion = $calificacion;

        return $this;
    }

    /**
     * Get Calificacion
     *
     * @return integer 
     */
    public function getCalificacion()
    {
        return $this->Calificacion;
    }

    /**
     * Set Estado
     *
     * @param boolean $estado
     * @return Historial
     */
    public function setEstado($estado)
    {
        $this->Estado = $estado;

        return $this;
    }

    /**
     * Get Estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Set IdResidencia
     *
     * @param \XPS\SSRBundle\Entity\Residencia $idResidencia
     * @return Historial
     */
    public function setIdResidencia(\XPS\SSRBundle\Entity\Residencia $idResidencia = null)
    {
        $this->IdResidencia = $idResidencia;

        return $this;
    }

    /**
     * Get IdResidencia
     *
     * @return \XPS\SSRBundle\Entity\Residencia 
     */
    public function getIdResidencia()
    {
        return $this->IdResidencia;
    }
}
