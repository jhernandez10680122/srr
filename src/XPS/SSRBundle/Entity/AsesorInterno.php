<?php

namespace XPS\SSRBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AsesorInterno
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="XPS\SSRBundle\Entity\AsesorInternoRepository")
 */
class AsesorInterno
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ApPaterno", type="string", length=50)
     */
    private $ApPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="ApMaterno", type="string", length=50)
     */
    private $ApMaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=100)
     */
    private $Nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="Especialidad", type="string", length=100)
     */
    private $Especialidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="Telefono", type="integer", length=14)
     */
    private $Telefono;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Estado", type="boolean")
     */
    private $Estado;

    /**
     * @ORM\OneToMany(targetEntity="Residencia", mappedBy="asesorinterno")
     */
    protected $residencias;
 
    public function __construct()
    {
        $this->residencias = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ApPaterno
     *
     * @param string $apPaterno
     * @return AsesorInterno
     */
    public function setApPaterno($apPaterno)
    {
        $this->ApPaterno = $apPaterno;

        return $this;
    }

    /**
     * Get ApPaterno
     *
     * @return string 
     */
    public function getApPaterno()
    {
        return $this->ApPaterno;
    }

    /**
     * Set ApMaterno
     *
     * @param string $apMaterno
     * @return AsesorInterno
     */
    public function setApMaterno($apMaterno)
    {
        $this->ApMaterno = $apMaterno;

        return $this;
    }

    /**
     * Get ApMaterno
     *
     * @return string 
     */
    public function getApMaterno()
    {
        return $this->ApMaterno;
    }

    /**
     * Set Nombre
     *
     * @param string $nombre
     * @return AsesorInterno
     */
    public function setNombre($nombre)
    {
        $this->Nombre = $nombre;

        return $this;
    }

    /**
     * Get Nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set Especialidad
     *
     * @param string $especialidad
     * @return AsesorInterno
     */
    public function setEspecialidad($especialidad)
    {
        $this->Especialidad = $especialidad;

        return $this;
    }

    /**
     * Get Especialidad
     *
     * @return string 
     */
    public function getEspecialidad()
    {
        return $this->Especialidad;
    }

    /**
     * Set Telefono
     *
     * @param integer $telefono
     * @return AsesorInterno
     */
    public function setTelefono($telefono)
    {
        $this->Telefono = $telefono;

        return $this;
    }

    /**
     * Get Telefono
     *
     * @return integer 
     */
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set Estado
     *
     * @param boolean $estado
     * @return AsesorInterno
     */
    public function setEstado($estado)
    {
        $this->Estado = $estado;

        return $this;
    }

    /**
     * Get Estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Add residencias
     *
     * @param \XPS\SSRBundle\Entity\Residencia $residencias
     * @return AsesorInterno
     */
    public function addResidencia(\XPS\SSRBundle\Entity\Residencia $residencias)
    {
        $this->residencias[] = $residencias;

        return $this;
    }

    /**
     * Remove residencias
     *
     * @param \XPS\SSRBundle\Entity\Residencia $residencias
     */
    public function removeResidencia(\XPS\SSRBundle\Entity\Residencia $residencias)
    {
        $this->residencias->removeElement($residencias);
    }

    /**
     * Get residencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResidencias()
    {
        return $this->residencias;
    }
}
