<?php

namespace XPS\SSRBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Residencia
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="XPS\SSRBundle\Entity\ResidenciaRepository")
 */
class Residencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdResidencia", type="integer")
     */
    private $IdResidencia;
    
    
    /**
     * @ORM\OneToOne(targetEntity="Alumno")
     * @ORM\JoinColumn(name="NoDeControl", referencedColumnName="NoDeControl")
     **/

    private $NoDeControl;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Empresa", inversedBy="residencias")
     * @ORM\JoinColumn(name="IdEmpresa", referencedColumnName="id")
     */
    protected $IdEmpresa;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="AsesorInterno", inversedBy="residencias")
     * @ORM\JoinColumn(name="IdAsesorInterno", referencedColumnName="id")
     */
    protected $IdAsesorInterno;

    /**
     * @var string
     *
     * @ORM\Column(name="RecursosHumanos", type="string", length=255)
     */
    private $RecursosHumanos;

    /**
     * @var string
     *
     * @ORM\Column(name="NoOficialAsesorInterno", type="string", length=255)
     */
    private $NoOficialAsesorInterno;

    /**
     * @var string
     *
     * @ORM\Column(name="AsesorExterno", type="string", length=255)
     */
    private $AsesorExterno;

    /**
     * @var string
     *
     * @ORM\Column(name="NombreDeProyecto", type="string", length=255)
     */
    private $NombreDeProyecto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaDictamen", type="date")
     */
    private $FechaDictamen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaRevision1", type="date")
     */
    private $FechaRevision1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaRevision2", type="date")
     */
    private $FechaRevision2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FechaRevision3", type="date")
     */
    private $FechaRevision3;

    /**
     * @var string
     *
     * @ORM\Column(name="Observaciones", type="text")
     */
    private $Observaciones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Estado", type="boolean")
     */
    private $Estado;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set IdResidencia
     *
     * @param integer $idResidencia
     * @return Residencia
     */
    public function setIdResidencia($idResidencia)
    {
        $this->IdResidencia = $idResidencia;

        return $this;
    }

    /**
     * Get IdResidencia
     *
     * @return integer 
     */
    public function getIdResidencia()
    {
        return $this->IdResidencia;
    }

    /**
     * Set RecursosHumanos
     *
     * @param string $recursosHumanos
     * @return Residencia
     */
    public function setRecursosHumanos($recursosHumanos)
    {
        $this->RecursosHumanos = $recursosHumanos;

        return $this;
    }

    /**
     * Get RecursosHumanos
     *
     * @return string 
     */
    public function getRecursosHumanos()
    {
        return $this->RecursosHumanos;
    }

    /**
     * Set NoOficialAsesorInterno
     *
     * @param string $noOficialAsesorInterno
     * @return Residencia
     */
    public function setNoOficialAsesorInterno($noOficialAsesorInterno)
    {
        $this->NoOficialAsesorInterno = $noOficialAsesorInterno;

        return $this;
    }

    /**
     * Get NoOficialAsesorInterno
     *
     * @return string 
     */
    public function getNoOficialAsesorInterno()
    {
        return $this->NoOficialAsesorInterno;
    }

    /**
     * Set AsesorExterno
     *
     * @param string $asesorExterno
     * @return Residencia
     */
    public function setAsesorExterno($asesorExterno)
    {
        $this->AsesorExterno = $asesorExterno;

        return $this;
    }

    /**
     * Get AsesorExterno
     *
     * @return string 
     */
    public function getAsesorExterno()
    {
        return $this->AsesorExterno;
    }

    /**
     * Set NombreDeProyecto
     *
     * @param string $nombreDeProyecto
     * @return Residencia
     */
    public function setNombreDeProyecto($nombreDeProyecto)
    {
        $this->NombreDeProyecto = $nombreDeProyecto;

        return $this;
    }

    /**
     * Get NombreDeProyecto
     *
     * @return string 
     */
    public function getNombreDeProyecto()
    {
        return $this->NombreDeProyecto;
    }

    /**
     * Set FechaDictamen
     *
     * @param \DateTime $fechaDictamen
     * @return Residencia
     */
    public function setFechaDictamen($fechaDictamen)
    {
        $this->FechaDictamen = $fechaDictamen;

        return $this;
    }

    /**
     * Get FechaDictamen
     *
     * @return \DateTime 
     */
    public function getFechaDictamen()
    {
        return $this->FechaDictamen;
    }

    /**
     * Set FechaRevision1
     *
     * @param \DateTime $fechaRevision1
     * @return Residencia
     */
    public function setFechaRevision1($fechaRevision1)
    {
        $this->FechaRevision1 = $fechaRevision1;

        return $this;
    }

    /**
     * Get FechaRevision1
     *
     * @return \DateTime 
     */
    public function getFechaRevision1()
    {
        return $this->FechaRevision1;
    }

    /**
     * Set FechaRevision2
     *
     * @param \DateTime $fechaRevision2
     * @return Residencia
     */
    public function setFechaRevision2($fechaRevision2)
    {
        $this->FechaRevision2 = $fechaRevision2;

        return $this;
    }

    /**
     * Get FechaRevision2
     *
     * @return \DateTime 
     */
    public function getFechaRevision2()
    {
        return $this->FechaRevision2;
    }

    /**
     * Set FechaRevision3
     *
     * @param \DateTime $fechaRevision3
     * @return Residencia
     */
    public function setFechaRevision3($fechaRevision3)
    {
        $this->FechaRevision3 = $fechaRevision3;

        return $this;
    }

    /**
     * Get FechaRevision3
     *
     * @return \DateTime 
     */
    public function getFechaRevision3()
    {
        return $this->FechaRevision3;
    }

    /**
     * Set Observaciones
     *
     * @param string $observaciones
     * @return Residencia
     */
    public function setObservaciones($observaciones)
    {
        $this->Observaciones = $observaciones;

        return $this;
    }

    /**
     * Get Observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->Observaciones;
    }

    /**
     * Set Estado
     *
     * @param boolean $estado
     * @return Residencia
     */
    public function setEstado($estado)
    {
        $this->Estado = $estado;

        return $this;
    }

    /**
     * Get Estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Set NoDeControl
     *
     * @param \XPS\SSRBundle\Entity\Alumno $noDeControl
     * @return Residencia
     */
    public function setNoDeControl(\XPS\SSRBundle\Entity\Alumno $noDeControl = null)
    {
        $this->NoDeControl = $noDeControl;

        return $this;
    }

    /**
     * Get NoDeControl
     *
     * @return \XPS\SSRBundle\Entity\Alumno 
     */
    public function getNoDeControl()
    {
        return $this->NoDeControl;
    }

    /**
     * Set IdEmpresa
     *
     * @param \XPS\SSRBundle\Entity\Empresa $idEmpresa
     * @return Residencia
     */
    public function setIdEmpresa(\XPS\SSRBundle\Entity\Empresa $idEmpresa = null)
    {
        $this->IdEmpresa = $idEmpresa;

        return $this;
    }

    /**
     * Get IdEmpresa
     *
     * @return \XPS\SSRBundle\Entity\Empresa 
     */
    public function getIdEmpresa()
    {
        return $this->IdEmpresa;
    }

    /**
     * Set IdAsesorInterno
     *
     * @param \XPS\SSRBundle\Entity\AsesorInterno $idAsesorInterno
     * @return Residencia
     */
    public function setIdAsesorInterno(\XPS\SSRBundle\Entity\AsesorInterno $idAsesorInterno = null)
    {
        $this->IdAsesorInterno = $idAsesorInterno;

        return $this;
    }

    /**
     * Get IdAsesorInterno
     *
     * @return \XPS\SSRBundle\Entity\AsesorInterno 
     */
    public function getIdAsesorInterno()
    {
        return $this->IdAsesorInterno;
    }
}
