<?php

namespace XPS\SSRBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Empresa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="XPS\SSRBundle\Entity\EmpresaRepository")
 */
class Empresa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=100)
     */
    private $Nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="Calle", type="string", length=100)
     */
    private $Calle;

    /**
     * @var string
     *
     * @ORM\Column(name="Localidad", type="string", length=100)
     */
    private $Localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="Colonia", type="string", length=100)
     */
    private $Colonia;

    /**
     * @var integer
     *
     * @ORM\Column(name="CP", type="integer")
     */
    private $CP;

    /**
     * @var string
     *
     * @ORM\Column(name="Telefono", type="string", length=30)
     */
    private $Telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="Correo", type="string", length=100)
     */
    private $Correo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Estado", type="boolean")
     */
    private $Estado;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Residencia", mappedBy="empresa")
     */
    protected $residencias;
 
    public function __construct()
    {
        $this->residencias = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Nombre
     *
     * @param string $nombre
     * @return Empresa
     */
    public function setNombre($nombre)
    {
        $this->Nombre = $nombre;

        return $this;
    }

    /**
     * Get Nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set Calle
     *
     * @param string $calle
     * @return Empresa
     */
    public function setCalle($calle)
    {
        $this->Calle = $calle;

        return $this;
    }

    /**
     * Get Calle
     *
     * @return string 
     */
    public function getCalle()
    {
        return $this->Calle;
    }

    /**
     * Set Localidad
     *
     * @param string $localidad
     * @return Empresa
     */
    public function setLocalidad($localidad)
    {
        $this->Localidad = $localidad;

        return $this;
    }

    /**
     * Get Localidad
     *
     * @return string 
     */
    public function getLocalidad()
    {
        return $this->Localidad;
    }

    /**
     * Set Colonia
     *
     * @param string $colonia
     * @return Empresa
     */
    public function setColonia($colonia)
    {
        $this->Colonia = $colonia;

        return $this;
    }

    /**
     * Get Colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->Colonia;
    }

    /**
     * Set CP
     *
     * @param integer $cP
     * @return Empresa
     */
    public function setCP($cP)
    {
        $this->CP = $cP;

        return $this;
    }

    /**
     * Get CP
     *
     * @return integer 
     */
    public function getCP()
    {
        return $this->CP;
    }

    /**
     * Set Telefono
     *
     * @param integer $telefono
     * @return Empresa
     */
    public function setTelefono($telefono)
    {
        $this->Telefono = $telefono;

        return $this;
    }

    /**
     * Get Telefono
     *
     * @return integer 
     */
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set Correo
     *
     * @param string $correo
     * @return Empresa
     */
    public function setCorreo($correo)
    {
        $this->Correo = $correo;

        return $this;
    }

    /**
     * Get Correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->Correo;
    }

    /**
     * Set Estado
     *
     * @param boolean $estado
     * @return Empresa
     */
    public function setEstado($estado)
    {
        $this->Estado = $estado;

        return $this;
    }

    /**
     * Get Estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Add residencias
     *
     * @param \XPS\SSRBundle\Entity\Residencia $residencias
     * @return Empresa
     */
    public function addResidencia(\XPS\SSRBundle\Entity\Residencia $residencias)
    {
        $this->residencias[] = $residencias;

        return $this;
    }

    /**
     * Remove residencias
     *
     * @param \XPS\SSRBundle\Entity\Residencia $residencias
     */
    public function removeResidencia(\XPS\SSRBundle\Entity\Residencia $residencias)
    {
        $this->residencias->removeElement($residencias);
    }

    /**
     * Get residencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResidencias()
    {
        return $this->residencias;
    }
}
