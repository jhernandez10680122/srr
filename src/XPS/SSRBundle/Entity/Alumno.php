<?php

namespace XPS\SSRBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alumno
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="XPS\SSRBundle\Entity\AlumnoRepository")
 */
class Alumno
{
    /**
     * @var string
     *
     * @ORM\Column(name="NoDeControl", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ApPaterno", type="string", length=50)
     */
    private $ApPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="ApMaterno", type="string", length=50)
     */
    private $ApMaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=100)
     */
    private $Nombre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Sexo", type="string", length=1)
     */
    private $Sexo;

    /**
     * @var integer
     *
     * @ORM\Column(name="Telefono", type="integer", length=14)
     */
    private $Telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="Correo", type="string", length=100)
     */
    private $Correo;

    /**
     * @var string
     *
     * @ORM\Column(name="Periodo", type="string", length=100)
     */
    private $Periodo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Estado", type="boolean")
     */
    private $Estado;
    

    /**
     * Set id
     *
     * @param integer $id
     * @return Alumno
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ApPaterno
     *
     * @param string $apPaterno
     * @return Alumno
     */
    public function setApPaterno($apPaterno)
    {
        $this->ApPaterno = $apPaterno;

        return $this;
    }

    /**
     * Get ApPaterno
     *
     * @return string 
     */
    public function getApPaterno()
    {
        return $this->ApPaterno;
    }

    /**
     * Set ApMaterno
     *
     * @param string $apMaterno
     * @return Alumno
     */
    public function setApMaterno($apMaterno)
    {
        $this->ApMaterno = $apMaterno;

        return $this;
    }

    /**
     * Get ApMaterno
     *
     * @return string 
     */
    public function getApMaterno()
    {
        return $this->ApMaterno;
    }

    /**
     * Set Nombre
     *
     * @param string $nombre
     * @return Alumno
     */
    public function setNombre($nombre)
    {
        $this->Nombre = $nombre;

        return $this;
    }

    /**
     * Get Nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set Sexo
     *
     * @param string $sexo
     * @return Alumno
     */
    public function setSexo($sexo)
    {
        $this->Sexo = $sexo;

        return $this;
    }

    /**
     * Get Sexo
     *
     * @return string 
     */
    public function getSexo()
    {
        return $this->Sexo;
    }

    /**
     * Set Telefono
     *
     * @param integer $telefono
     * @return Alumno
     */
    public function setTelefono($telefono)
    {
        $this->Telefono = $telefono;

        return $this;
    }

    /**
     * Get Telefono
     *
     * @return integer 
     */
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set Correo
     *
     * @param string $correo
     * @return Alumno
     */
    public function setCorreo($correo)
    {
        $this->Correo = $correo;

        return $this;
    }

    /**
     * Get Correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->Correo;
    }

    /**
     * Set Periodo
     *
     * @param string $periodo
     * @return Alumno
     */
    public function setPeriodo($periodo)
    {
        $this->Periodo = $periodo;

        return $this;
    }

    /**
     * Get Periodo
     *
     * @return string 
     */
    public function getPeriodo()
    {
        return $this->Periodo;
    }

    /**
     * Set Estado
     *
     * @param boolean $estado
     * @return Alumno
     */
    public function setEstado($estado)
    {
        $this->Estado = $estado;

        return $this;
    }

    /**
     * Get Estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->Estado;
    }
    
    
}
